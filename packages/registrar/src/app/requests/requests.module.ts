import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestsHomeComponent } from './components/requests-home/requests-home.component';
import {RequestsRoutingModule} from './requests.routing';
import {RequestsSharedModule} from './requests.shared';

@NgModule({
  imports: [
    CommonModule,
    RequestsSharedModule,
      RequestsRoutingModule
  ],
  declarations: [RequestsHomeComponent]
})
export class RequestsModule { }
