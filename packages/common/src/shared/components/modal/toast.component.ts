import {AfterViewInit, Component, ElementRef, Input, OnInit} from '@angular/core';
import {importExpr} from '@angular/compiler/src/output/output_ast';

declare var $: any;

/**
 *
 * A native spinner component
 * @export
 * @class SpinnerComponent
 * @implements {OnInit}
 */
@Component({
    selector: 'universis-toast.toast',
    template: `
                <div class="toast-header">
                    <strong class="toast-title mr-auto">{{title}}</strong>
                    <small class="toast-date">{{dateCreated | date: 'shortTime'}}</small>
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="toast-body" [innerHTML]="message">
                </div>
  `,
    styles: [
        `:host {
            z-index: auto;
        }`
    ]
})


export class ToastComponent implements OnInit, AfterViewInit {

    @Input() title: string;
    @Input() message: string;
    @Input() autoHide = true;
    @Input() delay = 5000;
    public dateCreated = new Date();

    private toast: any;

    constructor(private _element: ElementRef) { }

    ngOnInit() {

    }

    ngAfterViewInit(): void {
        import('bootstrap/js/dist/toast.js').then(Toast => {
            return new Toast(this._element.nativeElement, {
                animation: false,
                autohide: this.autoHide,
                delay: this.delay
            }).show();
        });
    }

    public show() {
        if (this.toast) {
            this.toast.show();
        }
    }

    public hide() {
        if (this.toast) {
            this.toast.hide();
        }
    }
}
