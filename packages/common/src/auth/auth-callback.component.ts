import {
  Component,
  ComponentFactoryResolver,
  OnInit,
  ViewChild,
  ViewContainerRef,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import { UserService } from './services/user.service';
import {ErrorService} from '../error/error.service';

@Component({
  selector: 'universis-auth-callback',
  template: '<div></div>',
  encapsulation: ViewEncapsulation.None,
})

export class AuthCallbackComponent implements OnInit {

  constructor(
    private _router: Router,
    private _userService: UserService,
    private _route: ActivatedRoute,
    private _errorService: ErrorService,
    private _authService: AuthenticationService) {
  }

  ngOnInit() {
    // validate code
    this._route.queryParamMap.subscribe((paramMap: ParamMap) => {
      this._authService.callback(paramMap.get('access_token')).then((res) => {
        if (typeof res === 'object') {
          return this._router.navigate(['/']);
        }
      }).catch( err => {
        return this._errorService.navigateToError(err);
      });
    });
  }

}
